import { NgModule } from '@angular/core';
import { AuthGuard } from './auth/auth.guard';
import { Routes, RouterModule, CanActivate } from '@angular/router';
import { AuthComponent } from './auth/auth.component';
import { PageContainerComponent } from './page-container/page-container.component';

const routes: Routes = [
  { path: 'login', component: AuthComponent },
  { path: 'questions', component: PageContainerComponent, canActivate: [AuthGuard] },
  { path: '**', component: AuthComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class AppRoutingModule { }
