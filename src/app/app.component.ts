import { Component, OnInit } from '@angular/core';
import { AuthService } from './auth/auth.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  status = false;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.authService.authChange.subscribe(status => {
      this.status = status;
      if(status === true) {
        this.router.navigate(['/questions']);
      } else {
        this.router.navigate(['/login']);
      }
    });

  }
  onLogoutClick() {
    this.authService.logoutUser()
  }

}
