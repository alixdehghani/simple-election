import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AuthService } from './auth.service';
import { AuthGuard } from './auth.guard';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']})
export class AuthComponent implements OnInit {
  status = true;
  loginForm: FormGroup;
  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.authService.authChange.subscribe(status => {
      this.status  = status;
    });
    this.loginForm = new FormGroup({
      email: new FormControl('', {
        validators: [Validators.required, Validators.email]
      }),
      password: new FormControl('', { validators: [Validators.required] })
    });
  }

  onSubmit() {
    this.authService.loginUser(this.loginForm.value.email, this.loginForm.value.password);
  }

}
