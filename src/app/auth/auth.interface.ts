export interface AuthInterface {
  username: string;
  password: string;
  isAdmin: boolean;
}
