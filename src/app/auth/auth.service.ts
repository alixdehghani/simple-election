
// import { AuthData } from './auth-data.model';
import { Subject, Subscription } from 'rxjs';
import { Injectable, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthInterface } from './auth.interface';
import { ICard } from '../card/card.interface';
// import { AngularFireAuth } from 'angularfire2/auth'
// import { TrainingService } from '../training/training.service';
// import { MatSnackBar } from '@angular/material';
// import { UiService } from '../share/ui.service';
@Injectable()
export class AuthService implements OnDestroy, OnInit {
  cards: Array<ICard>;
  authChange = new Subject<boolean>();
  isAdmin: boolean;
  currentUSer: string;
  isAuthentication = false;
  authSubscription: Subscription;
  logins: Array<AuthInterface>;
  constructor(private router: Router) {
    this.logins = [
      {
        username: 'a@a.com',
        password: '123456',
        isAdmin: false
      },
      {
        username: 'ali@ali.com',
        password: '123456',
        isAdmin: false
      },
      {
        username: 'admin@admin.com',
        password: '123456',
        isAdmin: true
      }
    ];
    this.cards = [
      // {
      //   id: 1,
      //   content: 'سوال 1 را جواب دهید',
      //   isBoolean: true,
      //   title: 'سوال1',
      //   usersPersent: [],
      //   usersVote: []
      // },
      // {
      //   id: 2,
      //   content: 'سوال 2 را جواب دهید',
      //   isBoolean: false,
      //   title: 'سوال2',
      //   usersPersent: [],
      //   usersVote: []
      // }
    ];
  }
  ngOnInit(){
    // this.cards = [
    //   {
    //     id: 1,
    //     content: 'سوال 1 را جواب دهید',
    //     isBoolean: true,
    //     title: 'سوال1',
    //     usersPersent: [],
    //     usersVote: []
    //   },
    //   {
    //     id: 2,
    //     content: 'سوال 2 را جواب دهید',
    //     isBoolean: false,
    //     title: 'سوال2',
    //     usersPersent: [],
    //     usersVote: []
    //   }
    // ];

  }
  // initAuthLisiner() {
  //     this.authSubscription = this.afAuth.authState.subscribe(user => {
  //         if (user) {
  //             this.isAuthentication = true;
  //             this.authChange.next(true);
  //             this.router.navigate(['/training']);
  //         } else {
  //             this.trainingService.canselSubscriptions();
  //             this.authChange.next(false);
  //             this.isAuthentication = false;
  //             this.router.navigate(['/login']);
  //         }
  //     });
  // }
  // registerUser(authData: AuthData) {
  //     this.uiService.loadingSpining.next(true);
  //     this.afAuth.auth.createUserWithEmailAndPassword(authData.email, authData.password).then(massage =>{
  //         // console.log(massage);
  //         this.uiService.loadingSpining.next(false);
  //     }).catch(error => {
  //         // console.log(error);
  //         this.uiService.loadingSpining.next(false);
  //         this.uiService.openSnackbar(error.message,null,2000);
  //     });
  // }
  loginUser(user, pass) {
    let faild = true;
    this.logins.forEach(login => {
      if (login.username === user && login.password === pass) {
        faild = false;
        this.authChange.next(true);
        this.currentUSer = user;
        this.isAuthentication = true;
        if (login.isAdmin === true) {
          this.isAdmin = true;
        } else {
          this.isAdmin = false;
        }
      }
    });
    if (faild) {
      this.authChange.next(false);
      this.currentUSer = null;
    }
  }


  logoutUser() {
    this.authChange.next(false);
    this.isAdmin = false;
    this.isAuthentication = false;
    this.currentUSer = null;
  }



  isAuth() {
    return this.isAuthentication;
  }

  ngOnDestroy() {
    if (this.authSubscription) {
      this.authSubscription.unsubscribe();
    }
  }

}
