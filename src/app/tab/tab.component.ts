import { AuthService } from './../auth/auth.service';
import { Component, OnInit, AfterViewChecked } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ICard } from '../card/card.interface';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.scss']
})
export class TabComponent implements OnInit, AfterViewChecked {
  totalVoit: number;
  totalPersent: number;
  favoriteSeason: string;
  seasons: string[] = ['بلی/خیر', 'درصدی'];
  currentUser: string;
  formGroup: FormGroup;
  constructor(public authService: AuthService, private _snackBar: MatSnackBar) {

  }
  onPersent(event) {
    const id = parseInt(event.split('-')[0]);
    let persent;
    persent = parseInt(event.split('-')[1]);

    this.authService.cards.forEach(card => {
      if (card.id === id) {
        let find = false;
        card.usersPersent.forEach(user => {
          if (user.username === this.currentUser) {
            find = true;
            if (persent >= 0) {
              user.persent = persent;
            } else {
              user.persent = -1;
            }
          }
        });
        if (!find && persent !== 'n') {
          card.usersPersent.push({
            username: this.currentUser,
            persent: persent
          });
        }
      }
    });

    console.log(this.authService.cards);

  }
  onVote(event: string) {
    const id = parseInt(event.split('-')[0]);
    let vote;
    if (event.split('-')[1] === 't') {
      vote = true;
    } else if (event.split('-')[1] === 'f') {
      vote = false;
    } else {
      vote = 'n'
    }
    this.authService.cards.forEach(card => {
      if (card.id === id) {
        let find = false;
        card.usersVote.forEach(user => {
          if (user.username === this.currentUser) {
            find = true;
            if (vote !== 'n') {
              user.vote = vote;
            } else {
              user.vote = null;
            }
          }
        });
        if (!find && vote !== 'n') {
          card.usersVote.push({
            username: this.currentUser,
            vote: vote
          });
        }
      }
    });
    console.log(this.authService.cards);

  }
  ngAfterViewChecked(): void {
    //Called after every check of the component's view. Applies to components only.
    //Add 'implements AfterViewChecked' to the class.
    this.currentUser = this.authService.currentUSer;

  }
  ngOnInit() {

    this.currentUser = this.authService.currentUSer;

    this.authService.authChange.subscribe(() => {
      this.currentUser = this.authService.currentUSer;
    });

    this.formGroup = new FormGroup({
      title: new FormControl('', {
        validators: [Validators.required]
      }),
      content: new FormControl('', { validators: [Validators.required] }),
      date: new FormControl('', { validators: [Validators.required] })
    });
  }

  calnegative(id: number): number {
    let positive = 0;
    let negative = 0;
    this.authService.cards.forEach(card => {
      if (card.id === id && card.isBoolean) {
        card.usersVote.forEach(vote => {
          if (vote.vote) {
            if (vote.username === this.currentUser) {
              card.checked = true;
            }
            else {
              card.checked = false;
            }
            positive = positive + 1;
          } else if (vote.vote != null) {
            if (vote.username === this.currentUser) {
              card.checked = true;
            }
            else {
              card.checked = false;
            }
            negative = negative + 1;
          }
        });
      }
    });
    return negative;
  }
  calpositive(id: number): number {
    let positive = 0;
    let negative = 0;
    this.authService.cards.forEach(card => {
      if (card.id === id && card.isBoolean) {
        card.usersVote.forEach(vote => {
          if (vote.vote) {
            positive = positive + 1;
          } else {
            negative = negative + 1;
          }
        });
      }
    });
    return positive;
  }
  calpersent(id: number): number {
    let persent = 0;
    let counter;
    let nanCounter = 0;
    this.authService.cards.forEach(card => {

      if (card.id === id && !card.isBoolean) {
        counter = card.usersPersent.length;
        card.usersPersent.forEach(p => {
          if (p.persent === -1) {
            nanCounter = nanCounter + 1;
          } else {
            if (p.username === this.currentUser) {
              card.checked = true;
            }
            else {
              card.checked = false;
            }
            persent = persent + p.persent;
          }

        });
      }

    });
    return persent / (counter - nanCounter);
  }

  radioclick: string;
  onRadioClick(event) {
    this.radioclick = event.value;

  }

  calID(): number {
    return this.authService.cards.length + 1;
  }
  onSubmit() {
    console.log(this.formGroup);

    let card: ICard;
    let bool = true;
    if (this.radioclick === 'درصدی') {
      bool = false
    }
    card =
      {
        id: this.calID(),
        content: this.formGroup.value.content,
        isBoolean: bool,
        title: this.formGroup.value.title,
        expireDate: this.formGroup.value.date,
        usersPersent: [],
        usersVote: [],
        usersComment: []
      }

    this.authService.cards.push(card);
    this.formGroup.reset();
    this._snackBar.openFromComponent(PizzaPartyComponent, {
      duration: 5000,
    });
  }

}

@Component({
  selector: 'snack-bar-component-example-snack',
  templateUrl: 'snack-bar-component-example-snack.html',
  styles: [`
    .example-pizza-party {
      color: hotpink;
    }
  `],
})
export class PizzaPartyComponent { }
