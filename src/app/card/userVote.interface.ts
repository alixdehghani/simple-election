export interface IUSerVote {
  username: string;
  vote: boolean;
}
