export interface IUSerComment {
  username: string;
  comment: string;
}
