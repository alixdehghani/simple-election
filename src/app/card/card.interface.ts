import { IUserPwersent } from './userPersent.interafce';
import { IUSerVote } from './userVote.interface';
import { IUSerComment } from './userComment.interface';

export interface ICard {
  id: number;
  title: string;
  subTitle?: string;
  header?: string;
  imgUrl?: string;
  content: string;
  isBoolean: boolean;
  expireDate: Date;

  checked?: boolean;
  usersVote?: Array<IUSerVote>;
  usersPersent?: Array<IUserPwersent>;
  usersComment?: Array<IUSerComment>;
}
