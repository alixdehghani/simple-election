import { IUserPwersent } from './userPersent.interafce';
import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { IUSerVote } from './userVote.interface';
import { IUSerComment } from './userComment.interface';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input() title: string;
  @Input() subTitle: string;
  @Input() header: string;
  @Input() imgUrl: string;
  @Input() content: string;
  @Input() isBoolean: boolean;
  @Input() totalpositive: number;
  @Input() totalnegative: number;
  @Input() totalPersent: number;
  @Input() id: number;
  @Input() date: Date;
  @Input() ExpiredTab: boolean;
  @Input() CurrentTab: boolean;
  voted: number;
  persent: number;
  @Input() checked: boolean;
  detailchecked = false;
  @Output() onVote = new EventEmitter<string>();
  @Output() onPersent = new EventEmitter<string>();
  @Output() onComment = new EventEmitter<string>();
  expire: boolean;
  constructor(public authService: AuthService) { }
  favoriteSeason: string;
  seasons: string[] = ['خیر', 'بله'];
  usersVoted = new Array<IUSerVote>();
  usersPersented = new Array<IUserPwersent>();
  usersComment = new Array<IUSerComment>();
  comment: string;
  ngOnInit() {
    if (new Date > this.date) {
      this.expire = true;
    }
    this.authService.cards.forEach(card => {
      if(card.id === this.id) {
        this.usersComment = card.usersComment;
        if(this.isBoolean) {
          card.usersVote.forEach(user => {
            this.usersVoted.push(user)
          });

        } else {
          card.usersPersent.forEach(user => {
            this.usersPersented.push(user)
          });
        }
      }
    })
  }

  onSubmitComment() {
    this.authService.cards.forEach(card => {
      if(card.id === this.id) {
        card.usersComment.push({
          comment: this.comment,
          username: this.authService.currentUSer
        })
      }
    })
  }

  oncheckBoxChange() {
    if (!this.checked) {
      if (this.isBoolean) {
        this.onVote.emit(`${this.id}-n`);
      } else {
        this.onPersent.emit(`${this.id}-n`);
      }
    }
  }

  onVoteselect(event) {
    if (this.favoriteSeason === 'خیر') {
      this.onVote.emit(`${this.id}-f`);
    }

    if (this.favoriteSeason === 'بله') {
      this.onVote.emit(`${this.id}-t`);
    }
  }

  onPersentSelected(event) {
    this.onPersent.emit(`${this.id}-${event.value}`);
  }
}
